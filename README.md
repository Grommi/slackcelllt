# SlackCellLT
Development for aquiring long time data with SlackCell (https://markusrampp.eu/SlackCell/) using ESP32 and microSD card. It can be controlled via WiFi.

Features:
- Force recording 
- Force threshold hibernation mode
- WiFi for displaying max and current force, downloading data, controlling ESP32
- OTA (over the air) update

Planned features:
- RTC support
- Live force plot
- Switch to SSE for faster force update
- Full configuration via WiFi

![screenshot of the webapp](pics/screenshot.png)

## Materials
- NodeMCU ESP32 module
- HX711
- MicroSD card module
- Power

### Optional
- RTC module

## Setup

### Wiring

MicroSD |  ESP32 pin
---|---
CS   | 5
MOSI | 23
CLK  | 18
MISO | 19

HX711 |  ESP32 pin
---|---
SCK | 32;
DOUT | 35;

### Software

1. Copy all files in `sd` directory into the root directory of your microSD card.
2. Change `LOADCELL_OFFSET` and `LOADCELL_DIVIDER_N` for the right values of your loadcell.
2. Build and upload `main.cpp` to your ESP32
3. Insert microSD card in your module


## Usage

Power on your ESP32. To start WiFi press the boot button (GPIO 0) and connect with your device to SSID `SlackCell` and passphrase `slacklife`. Open `42.42.42.42` in your browser and you can control your SlackCellLT.


