/*

SlackCell code for long term recording with a sd card

*/

#include "HX711.h" //library for loadcell amplifier
#include <SPI.h>
#include "FS.h"
#include "SD.h"
#include "RTClib.h"
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include "esp_log.h"
#include <EEPROM.h>
#include <rom/rtc.h>
#include <AsyncElegantOTA.h>

static const char *TAG = "slackcell";
AsyncWebServer server(80);
AsyncWebSocket ws("/ws");
// Create an Event Source on /events
AsyncEventSource events("/events");
const char* ssid = "SlackCell";
const char* password = "slacklife";
IPAddress    apIP(42, 42, 42, 42);

bool wifi = false;
bool sleeper = true;
bool rtc_on = false;
bool writeByte = false;

// SPIClass SDSPI(HSPI);
// SD card pins sketch
//
//  / |1|2|3|4|5|6|7|8|
// | 0| | | | | | | | |
//
// 0: empty
// 1: CS
// 2: MOSI
// 3: GND
// 4: VDD
// 5: SCLK
// 6: GND
// 7: MISO
// 8: empty 

// MicroSD  ESP32 pin
// CS   5
// MOSI 23
// CLK  18
// MISO 19

// String filename = "/SlackCellLT_20210523_124400.csv";
String filename = "/SlackCellLT.csv";
int force_limit = -50000;

const int baud = 115200;
// HX711 circuit wiring
const int LOADCELL_SCK_PIN = 32;
const int LOADCELL_DOUT_PIN = 35;
const int LOADCELL_OFFSET = 0;
const int LOADCELL_DIVIDER_N = 1;
// const int LOADCELL_DIVIDER_kg = LOADCELL_DIVIDER_N * 9.81;
// const int LOADCELL_DIVIDER_lb = LOADCELL_DIVIDER_N * 4.448;


HX711 loadcell; //setup HX711 object

unsigned long timestamp = 0;
int maxForce = 0;
int force = 0;
int prevForce = -100;
//bool recording = false;
//char prev_cmd = '0';
#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
int time_to_sleep =  10 * 60;        /* Time ESP32 will go to sleep (in seconds) */
RTC_DS3231 rtc;
String sdMessage;
#define RESET_EEPROM false

void appendFile(fs::FS &fs, String path, String message) {
  File file = fs.open(filename, FILE_APPEND);
  if(!file) {
    ESP_LOGE(TAG, "Failed to open file for appending");
    return;
  }
  
  if(!file.print(message)) {
      ESP_LOGE(TAG, "Append failed");
  }
  file.close();
}
void setupSDCard()
{
  ESP_LOGI(TAG, "Setting up SD card");
    //SDSPI.begin(MY_SCLK, MY_MISO, MY_MOSI, MY_CS);
    //Assuming use of SPI SD card
    //if (!SD.begin(MY_CS, SDSPI)) {
    if (!SD.begin()) {
      for(int i=0; i<100; i++){
        ESP_LOGE(TAG, "Card Mount Failed");
      }
      esp_restart();
    }
  
}

void appendFileByte(fs::FS &fs, String path, unsigned int timestamp, int force) {
  
  if(!SD.exists(path)){
    File file = SD.open(path, FILE_APPEND);
    appendFile(SD, path, "time,force\n");
  }
  File file = fs.open(path.c_str(), FILE_APPEND);
  if(!file) {
    ESP_LOGE(TAG, "Failed to open file for appending");
    return;
  }
  byte sendBuffer[7];
  sendBuffer[0] = timestamp & 0xFF;
  sendBuffer[1] = (timestamp  >> 8) & 0xFF;
  sendBuffer[2] = (timestamp  >> 16) & 0xFF;
  sendBuffer[3] = (timestamp  >> 24) & 0xFF;
  sendBuffer[4] = force & 0xFF;
  sendBuffer[5] = (force  >> 8) & 0xFF;
  sendBuffer[6] = 10;

  if(!file.write(sendBuffer, sizeof(sendBuffer))) {
      ESP_LOGE(TAG, "Append failed");
  }
  file.close();
}

void deleteFile(fs::FS &fs,  String path){
  Serial.println("Deleting file: " + path);
  if(fs.remove(path.c_str())){
    Serial.println("File deleted");
  } else {
    Serial.println("Delete failed");
  }
  appendFile(SD, filename, "");
}

void writeSD(unsigned int timestamp, int data) {
  const char* file = filename.c_str();
  if(writeByte){
    appendFileByte(SD, file, timestamp, data);
  }
  else{
    String sdMessage = String(timestamp) + "," + String(data) + "\n";
    appendFile(SD, file, sdMessage.c_str());
  }
}

void startWifi(){
  
  WiFi.mode(WIFI_AP_STA);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0)); 
  
  WiFi.softAP(ssid, password);
  server.begin();
}

String processor(const String& var){
  /**
    * Replaces placeholders in String to serve as HTML on the webserver
    *
    * @param var String with placeholders. Placeholders are between  %...%
    * @return String with replacements
    */
  if(var == "force"){
    String data = String(force);
    return data;
  }
  if(var == "max_force"){
    String data = String(maxForce);
    return data;
  }
  if(var == "threshold"){
    String data = String(force_limit);
    return data;
  }
  if(var == "sleep"){
    String data = String(time_to_sleep);
    return data;
  }
  return String();
}

void setForceLimit(int limit){
  ESP_LOGI(TAG, "Set force_limit to: %i", force_limit);
  force_limit = limit;
  EEPROM.put(0, limit);
  EEPROM.commit();
}

void readForceLimit(){
  EEPROM.get(0, force_limit);
}

void handleWebSocketMessage(void *arg, uint8_t *data, size_t len) {
  AwsFrameInfo *info = (AwsFrameInfo*)arg;
  if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {
    data[len] = 0;
    String str = (char*)data;
    switch(data[0]){
      case 'l':
        force_limit = str.substring(1).toInt();
        Serial.println(force_limit);
        setForceLimit(force_limit);
        break;
      case 'd':
        deleteFile(SD, filename);
        break;
      case 'r':
        ESP.restart();
        break;
      case 'w':
        wifi = false;
        if (wifi){
          startWifi();
        }
        else{
          server.end();
          WiFi.mode(WIFI_OFF);
        }
        break;
      default:
        Serial.print("Unknown command from websocket: ");
        Serial.println(str);
        break;
    }
  }
}

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type,
 void *arg, uint8_t *data, size_t len) {
  switch (type) {
    case WS_EVT_CONNECT:
      ws.textAll("Connected to SlackCellLT");
      Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
      break;
    case WS_EVT_DISCONNECT:
      Serial.printf("WebSocket client #%u disconnected\n", client->id());
      break;
    case WS_EVT_DATA:
      handleWebSocketMessage(arg, data, len);
      break;
    case WS_EVT_PONG:
    case WS_EVT_ERROR:
      break;
  }
}


void setupServer(){
  ESP_LOGI(TAG, "Setting up Server");
  
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SD, "/index.html", "text/html", false, processor);
  });
  server.on("/data", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SD, filename, "text/plain", true);
  });

  server.serveStatic("/", SD, "/");
  ws.onEvent(onEvent);
  server.addHandler(&ws);

  AsyncElegantOTA.begin(&server);
}

void slackcellSleep(){
  if (sleeper){
    ESP_LOGI(TAG, "Force: %i", force);
    ESP_LOGI(TAG, "Sleeping...");
    loadcell.power_down();
    esp_deep_sleep_start();
  }
}

void setupRTC(){
  ESP_LOGI(TAG, "Setting up RTC");
  if (! rtc.begin()) {
    ESP_LOGE(TAG, "Couldn't find RTC");
    return;
  }
  if (rtc.lostPower()) {
    ESP_LOGI(TAG, "RTC lost power, lets set the time!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
  DateTime now = rtc.now();
  // filename = "/SlackCellLT_" + String(now.toString("YYYYMMDDhhmmss")) + ".csv";
  // Serial.println(filename);
}


void setupForceLimit(){
  readForceLimit();
  // if force_limit is not written in EEPROM 
  if(force_limit == 0){
    ESP_LOGI(TAG, "EEPROM is empty. Writing force_limit");
    setForceLimit(force_limit);
  }
  else{
    ESP_LOGI(TAG, "Reading force_limit from EEPROM");
  }
  ESP_LOGI(TAG, "force_limit: %i", force_limit);
}

void setup() {
  pinMode(0, INPUT);
  EEPROM.begin(512);
  esp_log_level_set("*", ESP_LOG_VERBOSE); 
  Serial.begin(baud);
  Serial.println("Welcome to SlackCell!");
  delay(500);
  // Serial.print("Sketch:   ");   Serial.println(__FILE__);
  // Serial.print("Uploaded: ");   Serial.println(__DATE__);

  setupForceLimit();

  loadcell.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  loadcell.set_offset(LOADCELL_OFFSET);
  loadcell.set_scale(LOADCELL_DIVIDER_N);
  force = loadcell.get_units(30);
  
  // tare loadcell if force is small
  if (force > -50 && force < 50) {
    loadcell.tare();
    force = loadcell.get_units(30);
  }

  setupSDCard();
  setupServer();
  // start WiFi if boot button is pressed on second blink after reset button press
  if(digitalRead(0) == 0 || wifi){
    server.begin();
    sleeper = false;
    wifi = true;
  }
  
  esp_sleep_enable_timer_wakeup(time_to_sleep * uS_TO_S_FACTOR);
  esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);

  setupRTC();

  if (force < force_limit){
    slackcellSleep();
  }
  
}
long prev_time = millis();
int counter = 0;
void loop() {
  // ESP_LOGV(TAG, now.unixtime());
  
  // Switch Wifi on or off with boot button press
  if(digitalRead(0) == 0){
    startWifi();
    wifi = !wifi;
    sleeper = !wifi;
    if (wifi){
      server.begin();
    }
    else{
      server.end();
    }
    // delay to not trigger it multiple times with one press
    delay(600);
  }
  if (loadcell.is_ready()) {
    force = loadcell.get_units(1);
    timestamp = (unsigned int)millis(); 
    //milliseconds since startup
    // Serial.println(force); //prints first sigfig of force
    // Serial.print(" N"); //change depending on divider used
    // Serial.println();
    // Serial.println(timestamp);
    if (force > maxForce){
      maxForce = force;
    }
    if (wifi){
      counter++;
      ws.cleanupClients();
      AsyncElegantOTA.loop();
      // restrict number
      if (counter % 8 == 0 && force != -138){
        ws.textAll(String(timestamp) + "," + String(force));
      }
    }
    
    writeSD(timestamp, force);
    if (force < force_limit){
      // Serial.println(force);
      // Serial.println("Sleeping...");
      slackcellSleep();
    }
  }
}
