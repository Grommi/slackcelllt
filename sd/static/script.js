// data to be plotted
let data = [
  [],    // x-values (timestamps)
  [],    // y-values (force)
];

// timespan for plot in seconds
let timespan = 10

let hostname = window.location.hostname


var gateway = `ws://${hostname}/ws`;

// define gateway for local testing with python script
if (hostname == ""){
  gateway = "ws://localhost:8765"
}
  var websocket;
  function initWebSocket() {
    console.log('Trying to open a WebSocket connection on ' + gateway + '...');
    websocket = new WebSocket(gateway);
    websocket.onopen    = onOpen;
    websocket.onclose   = onClose;
    websocket.onmessage = onMessage;
  }
  function onOpen(event) {
    console.log('Connection opened');
    document.getElementById("connection").style.backgroundColor = "green";
  }

  function onClose(event) {
    document.getElementById("connection").style.backgroundColor = "red";
    console.log('Connection closed');
    setTimeout(initWebSocket, 2000);
  }

  function onMessage(event) {
    let timestamp = event.data.split(",")[0]
    let force = event.data.split(",")[1]
    document.getElementById('force').innerHTML = force;
    data[0].push(timestamp)
    data[1].push(force/1000)
    if (data[0].length > 10 * timespan){
      data[0].shift()
      data[1].shift()
    }
    
  // let data1 = sliceData(start1, start1 + len1);
  uplot.setData(data);
  }

function get_input_number(id, default_value){
let value = parseInt(document.getElementById(id).value);
        if(value){
            return value
        }else{
            return default_value
        }
}

function set_force_threshold(){
  var force_threshold = get_input_number("force_threshold", -5000);
  console.log(force_threshold);
  websocket.send("l" + force_threshold)
}

function delete_data(){
  if (confirm('Are you sure you want to delete the data?')) {
    console.log("Delete data");
    websocket.send("d");
  }
}
function disable_wifi(){
    console.log("Disable WiFi");
    websocket.send("w");
}
function restart(){
    console.log("Disable WiFi");
    websocket.send("r");
}

window.addEventListener('load', onLoad);

function onLoad(event) {
    initWebSocket();
  }

  function sliceData(start, end) {
    let d = [];

    for (let i = 0; i < data.length; i++)
      d.push(data[i].slice(start, end));

    return d;
  }
function initChart(){
    let opts = {
    title: "Live Force",
    id: "chart_force",
    class: "my-chart",
    width: window.innerWidth - 30,
    height: window.innerHeight * 0.4,
    series: [
      {
        label: "Time [s]",
        value: (self, rawValue) => rawValue,
      },
      {
  
        spanGaps: false,
        // in-legend display
        label: "Force [kN]",
        value: (self, rawValue) => rawValue,
  
        // series style
        stroke: "red",
        width: 1,
        fill: "rgba(255, 0, 0, 0.05)",
      }
    ],
    scales: {
      "x": {
        time: false,
      }
    },
    axes: [
      // x
      {
        stroke: "#aaa",
        label: "Time [s]",
        
        labelFont: "1rem Arial",
        
        grid: {
          show: false,
        },
      },
      // y
      {
        show: true,
        label: "Force [kN]",
        // labelSize: 30,
        labelFont: "1rem Arial",
        // font: "12px Arial",
        // gap: 5,
        // size: 50,
        stroke: "#aaa",
        
        grid: {
          show: true,
          stroke: "#444",
          width: 1,
        },
      }
    ]
  };
  let start1 = 1;
				let len1 = 2;

				let data1 = sliceData(start1, start1 + len1);
  return new uPlot(opts, data1, document.body);
}


let uplot = initChart();

// update data
  // let data1 = sliceData(start1, start1 + len1);
  // uplot.setData(data1);

